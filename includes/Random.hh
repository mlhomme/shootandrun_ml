#ifndef RANDOM_HH_
#define RANDOM_HH_

#include <time.h>
#include <stdlib.h>

class Random
{
public:
	Random();
	~Random();

	int 		my_random(int) const;
};

#endif