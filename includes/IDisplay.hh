#ifndef IDISPLAY_HH_
#define IDISPLAY_HH_

#include <utility>
#include <list>
#include <vector>

#include "ACase.hh"
#include "Map.hh"

typedef std::pair<Position, int>	Move;
typedef std::list<Move>				MoveArea;

class IDisplay
{
protected:
	/* detail for move */
	virtual bool		checkPosition(Position&, const Tab&, const MoveArea&, const Move&, bool) = 0;
	virtual Position	checkPossibility(const Tab&, const Move&, const MoveArea&, bool) = 0;
	virtual void		doArea(Map *, bool) = 0;

	/* aff case */
	virtual void		affUnit(AUnit *, const Position&) = 0;
	virtual	void		affCase(ACase *, const Position&) = 0;
	virtual	void		affCaseTinted(ACase *, const Position&) = 0;	

	/* do taff for case */
	virtual int			up(Map *, int) = 0;
	virtual int			down(Map *, int) = 0;
	virtual int			left(Map *, int) = 0;
	virtual int			right(Map *, int) = 0;
	virtual int			enter(Map *, int) = 0;
	virtual int			space(Map *, int) = 0;
	virtual int			alt(Map *, int) = 0;
	virtual int			keyC(Map *, int) = 0;
public:
	virtual ~IDisplay() {}

	/* aff map */
	virtual void		affMap(Map *) = 0;

	/* refresh and play event */
	virtual bool		refreshEvent(float) = 0;
	virtual int			playKey(Map *, int, bool) = 0;
	virtual int			doDetail(Map *, int, bool) = 0;

	/* getter */
	virtual const Position& 	getWhere() = 0;
	virtual const Position&		getStart() const = 0;
};

#endif