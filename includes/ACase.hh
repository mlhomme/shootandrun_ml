#ifndef ACASE_HH_
#define ACASE_HH_

#include "AUnit.hh"

class ACase
{
public:
	virtual ~ACase() {}
	
	virtual void		add(AUnit *) = 0;
	virtual void 		erase() = 0;
	virtual int			getType() const = 0;
	virtual AUnit		*getPtr() const = 0;
	virtual int			getSpeed() const = 0;
	virtual int			getRange() const = 0;
	virtual int			getDefense() const = 0;
	virtual int			getAttack() const = 0;
};

#endif