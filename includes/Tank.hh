#ifndef TANK_HH_
#define TANK_HH_

#include "Unit.hh"

class Tank : public Unit
{
public:
	Tank(const Position&, const int);
	virtual ~Tank();
	
};

#endif