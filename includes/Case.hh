#ifndef CASE_HH_
#define CASE_HH_

#include <string>

#include "ACase.hh"

const int			PLAIN = 0;
const int			MONTAIN = 1;
const int			RIVER = 2;
const int 			ROAD = 3;
const int 			RUINS = 4;
const int			FOREST = 5;

struct type_to_caract
{
	int			type;

	int			range;
	int			defense;
	int			speed;
	int			attack;
};

class Case : public ACase
{
	int			type;

	int			range;
	int			defense;
	int			speed;
	int			attack;

	AUnit		*ptr;
public:
	Case(int type);
	virtual ~Case();

	virtual void			add(AUnit *);
	virtual void			erase();
	virtual int				getType() const;
	virtual AUnit			*getPtr() const;
	virtual int				getSpeed() const;
	virtual int				getRange() const;
	virtual int				getDefense() const;
	virtual int				getAttack() const;
};

#endif