#ifndef PLAYERMANAGER_HH_
#define PLAYERMANAGER_HH_

#include <vector>

#include "Map.hh"
#include "IDisplay.hh"
#include "Display.hh"
#include "Player.hh"

/* typedef for tab player */
typedef std::vector<Player>		PlayerTab;

class PlayerManager
{
	PlayerTab			tab;
	Map					*map;
	IDisplay			*display;
public:
	PlayerManager(const Position&);
	~PlayerManager();
	
	int					game();
};

#endif