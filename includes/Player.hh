#ifndef PLAYER_HH_
#define PLAYER_HH_

#include <list>

#include "Tank.hh"
#include "Berserk.hh"
#include "Missile.hh"
#include "Militien.hh"
#include "Plane.hh"
#include "Rocket.hh"

#include "IDisplay.hh"
#include "Map.hh"
#include "AUnit.hh"

/* typedef list of unit */
typedef std::list<AUnit *> 			List;

/* typedef for FctPtr */

class Player
{
	List 			list;
	int				team;
	Map				*map;

public:
	Player(Map *, int);
	~Player();

	bool			iAmAlive();

	/* end of turn */
	void			refreshCapacity();

	/* action */
	void			attackHim(const Position &, const Position&);
	void			moveTo(const Position &, const Position&);
	void			doSkill(const Position &, const Position&);

	/* choose the right action to do */
	void			chooseAction(int, IDisplay *);
};

typedef	void	(Player::*FctPtr)(const Position&, const Position&);

#endif