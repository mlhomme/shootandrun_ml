#ifndef PLANE_HH_
#define PLANE_HH_

#include "Unit.hh"

class Plane : public Unit
{
public:
	Plane(const Position&, const int);
	virtual ~Plane();
	
};

#endif