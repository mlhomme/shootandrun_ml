#ifndef ROCKET_HH_
#define ROCKET_HH_

#include "Unit.hh"

class Rocket : public Unit
{
public:
	Rocket(const Position &, const int);
	virtual ~Rocket();
	
};

#endif