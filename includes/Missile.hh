#ifndef MISSILE_HH_
#define MISSILE_HH_

#include "Unit.hh"

class Missile : public Unit
{
public:
	Missile(const Position&, const int);
	virtual ~Missile();
	
};

#endif