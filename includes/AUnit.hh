#ifndef AUNIT_HH_
#define AUNIT_HH_

#include <utility>

class 		Map;

typedef std::pair<int, int> Position;

class AUnit
{
public:
	virtual ~AUnit() {};

/* action */

	virtual bool	canAttack(const Position&, Map *) const = 0;
	virtual void	attackHim(const Position&, Map *) = 0;
	virtual void	moveTo(const Position&, Map *) = 0;
	virtual void	doSkill(const Position&, Map *) = 0;

/* getter */

	/* status */
	virtual int					getUnitType() const = 0;
	virtual int					getTeam() const = 0;
	virtual int					getPV() const = 0;
	virtual int					getAttack() const = 0;
	virtual int					getSpeed() const = 0;
	virtual int					getMovement() const = 0;
	virtual int					getRange() const = 0;
	virtual const Position&		getPosition() const = 0;
	virtual int					getTimerSkill() const = 0;
	virtual int					getSkillType() const = 0;
	virtual int					getAnimation() const = 0;

	/* bool */
	virtual bool		getMove() const = 0;
	virtual bool		getShoot() const = 0;
	virtual bool 		getSkill() const = 0;

/* setter */

	/* status */
	virtual void					setPV(const int) = 0;
	virtual void					setAttack(const int) = 0;
	virtual void					setSpeed(const int) = 0;
	virtual void					setMovement(const int) = 0;
	virtual void					setRange(const int) = 0;
	virtual void					setPosition(const Position&) = 0;
	virtual void					setTimerSkill(const int) = 0;
	virtual void					setAnimation() = 0;

	/* bool */
	virtual void		setMove(const bool) = 0;
	virtual void		setShoot(const bool) = 0;
	virtual void 		setSkill(const bool) = 0;
};

#endif