#ifndef DISPLAY_HH_
#define DISPLAY_HH_

/* include allegro */
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

/* include cpp */
#include <list>
#include <string>
#include <vector>

/* include Interface */
#include "Unit.hh"
#include "IDisplay.hh"

/* typedef for use allegro */
typedef ALLEGRO_BITMAP 				Img;
typedef ALLEGRO_EVENT_QUEUE 		Event;
typedef ALLEGRO_FONT            	Font;
typedef ALLEGRO_DISPLAY 			Env;
typedef ALLEGRO_EVENT 				Ev;
typedef ALLEGRO_TIMEOUT				Timeout;
typedef ALLEGRO_SAMPLE*				Music;

typedef std::list<Position> 		AffMove;

typedef std::vector<Img *>			ImgTab;

/* define */
const int 							SIZEOFIMGUNIT = 6;
const int 							INDEX = 32;

/* where is plain */
const float 							xPLAIN = 0.0;
const float 							yPLAIN = 4 * INDEX;

/* where is montain */
const float 							xMONTAIN = 24 * INDEX;
const float 							yMONTAIN = 6 * INDEX;

/* where is river */
const float 							xRIVER = 18 * INDEX;
const float 							yRIVER = 16 * INDEX;

/* where is forest */
const float 							xFOREST = 22 * INDEX;
const float 							yFOREST = 7 * INDEX;

/* where is road */
const float 							xROAD = 21 * INDEX;
const float 							yROAD = 10 * INDEX;

/* where is ruins */
const float 							xRUINS = 13 * INDEX;
const float 							yRUINS = 7 * INDEX;

class Display : public IDisplay
{
	Position		position;
	Position 		start;
	Position 		positionToReturn;

	bool 			detail;
	Position  		where;
	AffMove 		list;

	Env 			*environnement;
	Event 			*event;
	Ev 				ev;
	ImgTab			unit;
	Img				*land;
	Music			mainSong;
	Music			attackSong;

	/* detail for move */
	virtual void		doArea(Map *, bool);
	virtual bool		checkPosition(Position&, const Tab&, const MoveArea&, const Move&, bool);
	virtual Position	checkPossibility(const Tab&, const Move&, const MoveArea&, bool);

	/* do taff for case */
	virtual int			up(Map *, int);
	virtual int			down(Map *, int);
	virtual int			left(Map *, int);
	virtual int			right(Map *, int);
	virtual int			enter(Map *, int);
	virtual int			space(Map *, int);
	virtual int			alt(Map *, int);
	virtual int			keyC(Map *, int);

	/* aff case */
	virtual void		affUnit(AUnit *, const Position&);
	virtual	void		affCase(ACase *, const Position&);
	virtual	void		affCaseTinted(ACase *, const Position&);
public:
	Display();
	virtual ~Display();

	/* aff map */
	virtual void 				affMap(Map *);

	/* refresh and  play evet */
	virtual bool				refreshEvent(float);
	virtual int 				playKey(Map *, int, bool);
	virtual int					doDetail(Map *, int, bool);

	/* getter */
	virtual const Position& 	getWhere();
	virtual const Position&		getStart() const;
};

typedef int (Display::*FctPtrKey)(Map *, int);

struct 			keyTab
{
	int 		key;
	FctPtrKey 	ptr;
};

struct 			indexTab
{
	int 		type;
	float 		defineX;
	float 		defineY;	
};

#endif