#ifndef BERSERK_HH_
#define BERSERK_HH_

#include "Unit.hh"

class Berserk : public Unit
{
public:
	Berserk(const Position &, const int);
	virtual ~Berserk();
	
};

#endif