#ifndef MILITIEN_HH_
#define MILITIEN_HH_

#include "Unit.hh"

class Militien : public Unit
{
public:
	Militien(const Position &, const int);
	virtual ~Militien();
	
};

#endif