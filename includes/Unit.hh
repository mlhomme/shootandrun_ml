#ifndef UNIT_HH_
#define UNIT_HH_

#include "AUnit.hh"

const int 			BERSERK = 0;
const int			TANK = 1;
const int			MISSILE = 2;
const int			MILITIEN = 3;
const int			PLANE = 4;
const int			ROCKET = 5;

const int			JUMP = 1;
const int			DOUBLE = 2;
const int			AREA = 3;

class Unit : public AUnit
{
protected:
/* status */

	int				team;
	int				unitType;
	int				pv;
	int				attack;
	int				speed;
	int				movement;
	int				range;
	Position		position;
	int				timerSkill;
	int				skillType;
	int				animation;

/* bool */

	bool			move;
	bool			shoot;
	bool			skill;
public:
	Unit(const int, const int, const int, const int, const int, const int, const Position&, const int);
	virtual ~Unit();

/* action */

	virtual bool	canAttack(const Position&, Map *) const;
	virtual void	attackHim(const Position&, Map *);
	virtual void	moveTo(const Position&, Map *);
	virtual void	doSkill(const Position&, Map *);

/* getter */

	/* status */
	virtual int					getUnitType() const;
	virtual int					getTeam() const;
	virtual int					getPV() const;
	virtual int					getAttack() const;
	virtual int					getSpeed() const;
	virtual int					getMovement() const;
	virtual int					getRange() const;
	virtual const Position&		getPosition() const;
	virtual int					getTimerSkill() const;
	virtual int					getSkillType() const;
	virtual int					getAnimation() const;

	/* bool */
	virtual bool		getMove() const;
	virtual bool		getShoot() const;
	virtual bool 		getSkill() const;

/* setter */

	/* status */
	virtual void					setPV(const int);
	virtual void					setAttack(const int);
	virtual void					setSpeed(const int);
	virtual void					setMovement(const int);
	virtual void					setRange(const int);
	virtual void					setPosition(const Position&);
	virtual void					setTimerSkill(const int);
	virtual void					setAnimation();

	/* bool */
	virtual void		setMove(const bool);
	virtual void		setShoot(const bool);
	virtual void 		setSkill(const bool);	
};

#endif