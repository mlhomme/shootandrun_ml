#ifndef MAP_HH_
#define MAP_HH_

#include <vector>

#include <iostream>

#include "Random.hh"
#include "ACase.hh"
#include "Case.hh"
#include "AUnit.hh"

typedef std::vector<ACase *>	Line;
typedef std::vector<Line>		Tab;

class Map
{
	Tab				map;

public:
	Map(const Position&);
	~Map();

	void			add(AUnit *);
	void			erase(AUnit *);	

	const Tab&		getTab() const;
	const ACase*	getCase(const Position&) const;
};

std::ostream&		operator<<(std::ostream&, const Map&);

#endif