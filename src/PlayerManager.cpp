#include "PlayerManager.hh"

PlayerManager::PlayerManager(const Position& position)
{
	map = new Map(position);
	display = new Display();

	Player 			p1(map, 0);
	Player 			p2(map, 1);

	tab.push_back(p1);
	tab.push_back(p2);
}

PlayerManager::~PlayerManager()
{
	delete display;
	delete map;
}

int					PlayerManager::game()
{
	bool			Exit = false;
	int				teamTurn = 0;
	int				i = 0;

	while (Exit == false)
	{
		if (display->refreshEvent(0.1) == true)
		{
			if ((i = display->playKey(map, teamTurn, false)) == -1)
				Exit = true;
			if (i == 4)
			{
				tab[teamTurn].refreshCapacity();
				if (tab[teamTurn].iAmAlive() == true)
				{
					Exit = true;
					teamTurn = teamTurn == 0 ? 1 : 0;
					std::cout << "Team " << teamTurn << " WIN !!!" << std::endl;
				}
				teamTurn = teamTurn == 0 ? 1 : 0;
			}
			else		
				tab[teamTurn].chooseAction(i, display);
		}
		display->affMap(map);
	}
	return (0);
}