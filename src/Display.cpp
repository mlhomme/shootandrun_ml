#include "Display.hh"

Display::Display() : IDisplay()
{
	/* init Allegro */
	if (!al_init())
	{
		std::string			error("Can't load Init");
		throw error;
	}

	/* init keyboard */
	if (!al_install_keyboard())
	{
		std::string			error("Can't load Keyboard");
		throw error;				
	}

	/* init image */
	if (!al_init_image_addon())
	{
		std::string			error("Can't load Image addon");
		throw error;		
	}

	/* Load environnement */
	environnement = al_create_display(1000, 840);

	if (!environnement)
	{
		std::string			error("Can't load Environnement");
		throw error;		
	}

	/* load event */
	event = al_create_event_queue();

	if (!event)
	{
		std::string			error("Can't load Event");
		throw error;		
	}

	/* add event source */
	al_register_event_source(event, al_get_display_event_source(environnement));
   	al_register_event_source(event, al_get_keyboard_event_source());

   	/* music */
   	if (!al_install_audio())
   	{
		std::string			error("Can't init audio");
		throw error;
   	}

   	if (!al_init_acodec_addon())
   	{
		std::string			error("Can't init acodec");
		throw error;
   	}

   	if (!al_reserve_samples(2))
   	{
 		std::string			error("Can't reserve samples");
		throw error;
   	}

   	mainSong = al_load_sample("./music/mainsong.flac");
   	if (!mainSong)
   	{
 		std::string			error("Can't load samples mainsong.flac");
		throw error;   		
   	}

   	attackSong = al_load_sample("./music/attack.flac");
   	if (!mainSong)
   	{
 		std::string			error("Can't load samples attack.flac");
		throw error;   		
   	}

   	al_play_sample(mainSong, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL);

	/* init of vector */
	ImgTab					buff1(12, NULL);
	ImgTab					buff2(3, NULL);

	/* overload operator= */
	unit = buff1;

	/* img unit */
	unit[0] = al_load_bitmap("./sprite/charBLUE.png");
	unit[1] = al_load_bitmap("./sprite/tankBLUE.png");
	unit[2] = al_load_bitmap("./sprite/artillerieBLUE.png");
	unit[3] = al_load_bitmap("./sprite/militienBLUE.png");
	unit[4] = al_load_bitmap("./sprite/plaineBLUE.png");
	unit[5] = al_load_bitmap("./sprite/rocketBLUE.png");
	unit[6] = al_load_bitmap("./sprite/charRED.png");
	unit[7] = al_load_bitmap("./sprite/tankRed.png");
	unit[8] = al_load_bitmap("./sprite/artillerieRed.png");
	unit[9] = al_load_bitmap("./sprite/militienRED.png");
	unit[10] = al_load_bitmap("./sprite/plaineRED.png");
	unit[11] = al_load_bitmap("./sprite/rocketRED.png");

	int 		i = 0;
	while (i != 12)
	{
		if (!(unit[i]))
		{
			std::cerr << "Image " << i << std::endl;
			std::string 			error("Can't load image unit");
			throw error;
		}
		++i;
	}

	/*img land */
	land = al_load_bitmap("./sprite/Map_parts.bmp");

	if (!land)
	{
		std::string 			error("Can't load land");
		throw error;
	}

   	position.first = 0;
   	position.second = 0;

   	start.first = 0;
   	start.second = 0;

   	detail = false;
   	where.first = 0;
   	where.second = 0;
}

Display::~Display()
{
	if (environnement != NULL)
		al_destroy_display(environnement);
	if (event != NULL)
		al_destroy_event_queue(event);
	
	int 			i = 0;
	while (i != 12)
	{
		if ((unit[i]) != NULL)
			al_destroy_bitmap(unit[i]);
		++i;
	}

	if (land != NULL)
		al_destroy_bitmap(land);
}

void				Display::affUnit(AUnit *ptr, const Position &position)
{
	al_draw_bitmap_region(unit[ptr->getUnitType() + (ptr->getTeam() == 1 ? 0 : SIZEOFIMGUNIT)]
		, 0 + (INDEX * ptr->getAnimation()), 0, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);
	ptr->setAnimation();
}

void				Display::affCase(ACase *Case, const Position &position)
{
	AUnit 			*ptr;
	indexTab 		tab[6] =
	{
		{PLAIN, xPLAIN, yPLAIN},
		{MONTAIN, xMONTAIN, yMONTAIN},
		{RIVER, xRIVER, yRIVER},
		{ROAD, xROAD, yROAD},
		{RUINS, xRUINS, yRUINS},
		{FOREST, xFOREST, yFOREST}
	};

	al_draw_bitmap_region(land, tab[Case->getType()].defineX, tab[Case->getType()].defineY, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);
	ptr = Case->getPtr();
	if (ptr != NULL)
		affUnit(ptr, position);
}

void				Display::affCaseTinted(ACase *Case, const Position &position)
{
	AUnit 			*ptr;
	indexTab 		tab[6] =
	{
		{PLAIN, xPLAIN, yPLAIN},
		{MONTAIN, xMONTAIN, yMONTAIN},
		{RIVER, xRIVER, yRIVER},
		{ROAD, xROAD, yROAD},
		{RUINS, xRUINS, yRUINS},
		{FOREST, xFOREST, yFOREST}
	};

	al_draw_tinted_bitmap_region(land, al_map_rgba_f(250, 0, 0, 0), tab[Case->getType()].defineX, tab[Case->getType()].defineY, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);

	ptr = Case->getPtr();
	if (ptr != NULL)
	{
		al_draw_bitmap_region(unit[ptr->getUnitType() + (ptr->getTeam() == 1 ? 0 : SIZEOFIMGUNIT)]
			, 0 + (INDEX * ptr->getAnimation()), 0, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);
		ptr->setAnimation();
	}
}

void				Display::affMap(Map *map)
{
	Position 		aff;
	Position 		pos;
	const Tab& 		tab = map->getTab();	
	const ACase*	Case;	

	pos.first = start.first;
	pos.second = start.second;
	al_clear_to_color(al_map_rgb(0,0,0));
	while (pos.second != static_cast<int>(tab.size()) && (pos.second - start.second) < 20)
	{
		aff.first = pos.first - start.first;
		aff.second = pos.second - start.second;
		affCase(tab[pos.second][pos.first], aff);
		++(pos.first);
		if (pos.first == static_cast<int>(tab[0].size()) || (pos.first - start.first) == 20)
		{
			++(pos.second);
			pos.first = 0;
		}
	}
	Case = tab[position.second][position.first];
	if (Case->getType() == PLAIN)
		al_draw_tinted_bitmap_region(land,  al_map_rgba_f(0, 250, 0, 0), xPLAIN, yPLAIN, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);
	else if (Case->getType() == MONTAIN)
		al_draw_tinted_bitmap_region(land,  al_map_rgba_f(0, 250, 0, 0), xMONTAIN, yMONTAIN, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);
	else
		al_draw_tinted_bitmap_region(land,  al_map_rgba_f(0, 250, 0, 0), xRIVER, yRIVER, INDEX, INDEX, position.first * INDEX, position.second * INDEX, 0);
	AffMove::const_iterator 		it = list.begin();
	Position 						posi;
	while (it != list.end())
	{
		posi = (*it);
		posi.first -= start.first;
		posi.second -= start.second;
		affCaseTinted(tab[(*it).second][(*it).first], posi);
		++it;
	}
	al_flip_display();
}

bool				Display::refreshEvent(float timer)
{
	Timeout 		timeout;
    al_init_timeout(&timeout, timer);

    return (al_wait_for_event_until(event, &ev, &timeout));
}

bool 							Display::checkPosition(Position& position, const Tab &tab, const MoveArea& moveArea, const Move& move, bool skill)
{
	AffMove::iterator				it = list.begin();
	MoveArea::const_iterator		it2 = moveArea.begin();

	while (list.end() != it)
	{
		if ((*it).first == position.first && (*it).second == position.second)
			return (false);
		++it;
	}
	while (moveArea.end() != it2)
	{
		if ((*it2).first.first == position.first && (*it2).first.second == position.second)
			return (false);
		++it2;
	}
	(void)skill;
	(void)move;
	if (tab[position.second][position.first]->getPtr() != NULL)
		return (false);
	return (true);
}

Position				Display::checkPossibility(const Tab &tab, const Move& move, const MoveArea& moveArea, bool skill)
{
   Position 		pos = move.first;

	if (move.first.first > 0)
		if (move.second >= (tab[move.first.second][move.first.first - 1]->getSpeed() + 1))
		{
			--(pos.first);
			if (checkPosition(pos, tab, moveArea, move, skill) == true)
				return (pos);
			if (tab[pos.second][pos.first]->getPtr() != NULL && skill == true)
			{
				--(pos.first);
				if (pos.first >= 0)
					if (checkPosition(pos, tab, moveArea, move, skill) == true)
						return (pos);
				++(pos.first);
			}
			++(pos.first);
		}
	if (move.first.first < static_cast<int>(tab[0].size()) - 1)
		if (move.second >= (tab[move.first.second][move.first.first + 1]->getSpeed() + 1))
			{
				++(pos.first);
				if (checkPosition(pos, tab, moveArea, move, skill) == true)
					return (pos);
				if (tab[pos.second][pos.first]->getPtr() != NULL && skill == true)
				{
					++(pos.first);
					if (pos.first < static_cast<int>(tab[0].size()))
						if (checkPosition(pos, tab, moveArea, move, skill) == true)
							return (pos);
					--(pos.first);
				}
				--(pos.first);
			}
	if (move.first.second != 0)
		if (move.second >= (tab[move.first.second - 1][move.first.first]->getSpeed() + 1))
			{
				--(pos.second);
				if (checkPosition(pos, tab, moveArea, move, skill) == true)
					return (pos);
				if (tab[pos.second][pos.first]->getPtr() != NULL && skill == true)
				{
					--(pos.second);
					if (pos.second >= 0)
						if (checkPosition(pos, tab, moveArea, move, skill) == true)
							return (pos);
					++(pos.second);
				}
				++(pos.second);
			}
	if (move.first.second < static_cast<int>(tab.size()) - 1)
		if (move.second >= (tab[move.first.second + 1][move.first.first]->getSpeed() + 1))
			{
				++(pos.second);
			if (checkPosition(pos, tab, moveArea, move, skill) == true)
				return (pos);
				if (tab[pos.second][pos.first]->getPtr() != NULL && skill == true)
				{
					++(pos.second);
					if (pos.second < static_cast<int>(tab.size()))
						if (checkPosition(pos, tab, moveArea, move, skill) == true)
							return (pos);
					--(pos.second);
				}
				--(pos.second);
			}

   pos.first = -1;
   pos.second = -1;
   return (pos);
}

void					Display::doArea(Map *map, bool skill)
{
	Move 				starter;
	MoveArea			list;
	MoveArea::iterator	it;
	MoveArea::iterator	buff;
	Position 			newP;
	const Tab 			&tab = map->getTab();

	starter.first = where;
	starter.second = tab[where.second][where.first]->getPtr()->getMovement();
	list.push_back(starter);
	while (list.size() != 0)
	{
		it = list.begin();
		while (it != list.end())
		{
			newP = checkPossibility(tab, *it, list, skill);
			while (newP.first != -1)
			{
				starter.first = newP;
				starter.second = (*it).second - 
				(tab[starter.first.second][starter.first.first]->getType() == RIVER ? 3 : 1);
				list.push_front(starter);
				newP = checkPossibility(tab, *it, list, skill);
			}
			this->list.push_back((*it).first);
			buff = it;
			++it;
			list.erase(buff);
		}
	}
}

int					Display::doDetail(Map *map, int team, bool skill)
{
	bool 			done = false;

	detail = !detail;
	positionToReturn.first = start.first + position.first;
	positionToReturn.second = start.second + position.second;	
	if (detail == false && list.size() != 0)
	{
		AffMove::const_iterator 		it = list.begin();

		while (it != list.end())
		{
			if ((*it).first == positionToReturn.first && (*it).second == positionToReturn.second)
			{
				done = true;
				break;
			}
			++it;
		}
		list.clear();
		return (done == true ? 1 : 0);
	}
	if (detail == false || map->getTab()[positionToReturn.second][positionToReturn.first]->getPtr() == NULL)
		return (0);
	if (map->getTab()[positionToReturn.second][positionToReturn.first]->getPtr()->getTeam() != team)
		return (0);
	where.first = positionToReturn.first;
	where.second = positionToReturn.second;
	if (map->getTab()[positionToReturn.second][positionToReturn.first]->getPtr()->getSkillType() == JUMP
		&& map->getTab()[positionToReturn.second][positionToReturn.first]->getPtr()->getSkill() == false)
		doArea(map, true);
	else
		doArea(map, false);		
	(void)skill;
	return (0);
}

int 				Display::up(Map *map, int team)
{
	if (position.second != 0)
		--(position.second);
	else if (position.second - start.second - 1 < 0 && start.second != 0)
	{
		--(start.second);
		++(position.second);
	}
	(void)team;
	(void)map;
	return (0);
}

int 				Display::down(Map *map, int team)
{
	if (position.second + start.second != (static_cast<int>(map->getTab().size()) - 1) && (position.second) != 19)
		++(position.second);
	else if (position.second == 19 && start.second != (static_cast<int>(map->getTab().size()) - 1))
	{
		++(start.second);
		--(position.second);
	}
	(void)team;
	return (0);
}

int 				Display::left(Map *map, int team)
{
	if (position.first != 0)
		--(position.first);
	else if (position.first - start.first - 1 < 0 && start.first != 0)
	{
		--(start.first);
		++(position.first);
	}
	(void)map;
	(void)team;
	return (0);
}

int 				Display::right(Map *map, int team)
{
	if (position.first + start.first != (static_cast<int>(map->getTab()[0].size()) - 1) && (position.first) < 19)
		++(position.first);
	else if (position.first == 19 && start.first != (static_cast<int>(map->getTab()[0].size()) - 1))
	{
		++(start.first);
		--(position.first);
	}
	(void)team;
	return (0);
}

int 				Display::enter(Map *map, int team)
{
	return (doDetail(map, team, false));
}

int 				Display::space(Map *map, int team)
{
	if (detail != true)
		return (0);
	doDetail(map, team, false);
   	al_play_sample(attackSong, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
	return (2);
}

int 				Display::alt(Map *map, int team)
{
	(void)map;
	(void)team;
	if (detail != false)
		return (0);
	return (4);
}

int 				Display::keyC(Map *map, int team)
{
	(void)map;
	(void)team;
	return (3);
}

int 				Display::playKey(Map *map, int team, bool skill)
{
	int 			i = 0;
	keyTab 			tab[8] =
	{
		{ALLEGRO_KEY_UP, &Display::up},
		{ALLEGRO_KEY_DOWN, &Display::down},
		{ALLEGRO_KEY_LEFT, &Display::left},
		{ALLEGRO_KEY_RIGHT, &Display::right},
		{ALLEGRO_KEY_ENTER, &Display::enter},
		{ALLEGRO_KEY_SPACE, &Display::space},
		{ALLEGRO_KEY_ALT, &Display::alt},
		{ALLEGRO_KEY_C, &Display::keyC}
	};

	if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		return (-1);
	(void)skill;
	if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
	{
		if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
			return (-1);
		while (i != 8 && tab[i].key != ev.keyboard.keycode)
			++i;
		if (i != 8)
			return ((this->*(tab[i]).ptr)(map, team));
	}
	return (0);
}

/* getter */
const Position& 	Display::getWhere()
{
	positionToReturn.first = start.first + position.first;
	positionToReturn.second = start.second + position.second;
	return (positionToReturn);
}

const Position&		Display::getStart() const
{
	return (where);
}