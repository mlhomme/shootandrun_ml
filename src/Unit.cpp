#include "Unit.hh"
#include "Map.hh"

Unit::Unit(const int team, const int unitType, const int pv, const int attack, const int speed, const int range, const Position& position, int skillType) : AUnit()
{
	this->team = team;
	this->unitType = unitType;
	this->pv = pv;
	this->attack = attack;
	this->speed = speed;
	this->movement = speed;
	this->range = range;
	this->animation = 0;

	this->position.first = position.first;
	this->position.second = position.second;

	this->timerSkill = 0;
	this->skillType = skillType;

	move = true;
	shoot = true;
	skill = true;
}

Unit::~Unit()
{

}

/* --------------------------------------------------------------------- */
/* ------------------action--------------------------------------------- */
/* --------------------------------------------------------------------- */

bool				Unit::canAttack(const Position& to, Map *map) const
{
	if (shoot == false)
		return (shoot);
	if (map->getTab()[to.second][to.first]->getPtr() == NULL)
		return (false);

	int				result = 0;
	int				bonusRange = 0;

	result = position.first - to.first;
	result += (position.second - to.second);
	
	bonusRange = map->getTab()[position.second][position.first]->getRange();
	if (result > (range + bonusRange))
		return (false);
	result *= -1;
	if (result > (range + bonusRange))
		return (false);			
	return (true);
}

void				Unit::attackHim(const Position& to, Map *map)
{
	if (canAttack(to, map) == false)
		return ;
	AUnit			*ptr = map->getTab()[to.second][to.first]->getPtr();

	int				dmg = map->getTab()[position.second][position.first]->getAttack();

	dmg += attack;
	dmg -= map->getTab()[position.second][position.first]->getDefense();

	if (skillType == DOUBLE && skill == false)
		dmg = dmg * 2;
	ptr->setPV(ptr->getPV() - dmg);
	if (ptr->getPV() <= 0)
		map->erase(ptr);
	shoot = !shoot;
}

void				Unit::moveTo(const Position& to, Map *map)
{
	if (move == false)
		return ;
	map->erase(this);
	position = to;
	map->add(this);
	move = false;
}

void				Unit::doSkill(const Position& useless, Map *map)
{
	(void)useless;
	(void)map;
	if (timerSkill != 0)
		return ;
	timerSkill = 4;
	skill = !skill;
}

/* --------------------------------------------------------------------- */
/* ------------------getter--for--status-------------------------------- */
/* --------------------------------------------------------------------- */

int					Unit::getUnitType() const
{
	return (unitType);
}

int					Unit::getTeam() const
{
	return (team);
}

int					Unit::getPV() const
{
	return (pv);
}

int					Unit::getAttack() const
{
	return (attack);
}

int					Unit::getSpeed() const
{
	return (speed);
}

int					Unit::getMovement() const
{
	return (movement);
}

int 				Unit::getRange() const
{
	return (range);
}

const Position&		Unit::getPosition() const
{
	return (position);
}

int 				Unit::getTimerSkill() const
{
	return (timerSkill);
}

int					Unit::getSkillType() const
{
	return (skillType);
}

int					Unit::getAnimation() const
{
	return (animation);
}

/* --------------------------------------------------------------------- */
/* -----------------getter--for--bool----------------------------------- */
/* --------------------------------------------------------------------- */

bool				Unit::getMove() const
{
	return (move);
}

bool 				Unit::getShoot() const
{
	return (shoot);
}

bool 				Unit::getSkill() const
{
	return (skill);
}

/* --------------------------------------------------------------------- */
/* -----------------setter--for--status--------------------------------- */
/* --------------------------------------------------------------------- */

void				Unit::setPV(const int pv)
{
	this->pv = pv;
}

void				Unit::setAttack(const int attack)
{
	this->attack = attack;
}

void				Unit::setSpeed(const int speed)
{
	this->speed = speed;
}

void				Unit::setMovement(const int movement)
{
	this->movement = movement;
}

void				Unit::setRange(const int range)
{
	this->range = range;
}

void				Unit::setPosition(const Position& position)
{
	this->position.first = position.first;
	this->position.second = position.second;
}

void				Unit::setTimerSkill(const int timerSkill)
{
	this->timerSkill = timerSkill;
}

void				Unit::setAnimation()
{
	this->animation = animation == 2 ? 0 : (animation + 1);
}
/* --------------------------------------------------------------------- */
/* -----------------setter--for--bool----------------------------------- */
/* --------------------------------------------------------------------- */

void				Unit::setMove(const bool move)
{
	this->move = move;
}

void				Unit::setShoot(const bool shoot)
{
	this->shoot = shoot;
}

void				Unit::setSkill(const bool skill)
{
	this->skill = skill;
}
