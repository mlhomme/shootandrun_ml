#include "Player.hh"

Player::Player(Map *map, int team)
{
	this->team = team;
	this->map = map;

	Position			pos;

	pos.first = 0;
	pos.second = team == 0 ? 0 : map->getTab()[0].size() - 1;
	list.push_back(new Berserk(pos, team));
	++pos.first;
	list.push_back(new Berserk(pos, team));
	++pos.first;
	list.push_back(new Tank(pos, team));
	++pos.first;
	list.push_back(new Tank(pos, team));
	++pos.first;
	list.push_back(new Missile(pos, team));
	++pos.first;
	list.push_back(new Missile(pos, team));
	++pos.first;
	list.push_back(new Militien(pos, team));
	++pos.first;
	list.push_back(new Plane(pos, team));
	++pos.first;
	list.push_back(new Rocket(pos, team));

	List::iterator			it = list.begin();
	while (it != list.end())
		{
			map->add(*it);
			++it;
		}
}

Player::~Player()
{

}

bool				Player::iAmAlive()
{
	List::iterator	it = list.begin();
	List::iterator	buff;

	if (list.size() == 0)
		return (true);
	while (it != list.end())
	{
		if ((*it)->getPV() <= 0)
		{
			buff = it;
			++it;
			list.erase(buff);
			continue ;
		}
		++it;
	}
	if (list.size() == 0)
		return (true);
	return (false);
}

void				Player::refreshCapacity()
{
	List::iterator	it = list.begin();

	while (it != list.end())
	{
		(*it)->setMove(true);
		(*it)->setShoot(true);
		(*it)->setSkill(true);
		if ((*it)->getTimerSkill() != 0)
			(*it)->setTimerSkill((*it)->getTimerSkill() - 1);
		++it;
	}	
}

void				Player::moveTo(const Position &start, const Position &to)
{
	List::iterator	it = list.begin();

	while (it != list.end())
	{
		const Position		&pos = (*it)->getPosition();
		if (pos.first == start.first && pos.second == start.second)
			(*it)->moveTo(to, map);
		++it;
	}	
}

void				Player::attackHim(const Position &start, const Position &to)
{
	List::iterator	it = list.begin();

	while (it != list.end())
	{
		const Position		&pos = (*it)->getPosition();
		if (pos.first == start.first && pos.second == start.second)
			(*it)->attackHim(to, map);
		++it;
	}	
}

void				Player::doSkill(const Position &useless, const Position &usefull)
{
	(void)useless;
	List::iterator	it = list.begin();

	while (it != list.end())
	{
		const Position		&pos = (*it)->getPosition();
		if (pos.first == usefull.first && pos.second == usefull.second)
			(*it)->doSkill(useless, map);
		++it;
	}
}

void				Player::chooseAction(int action, IDisplay *display)
{
	const FctPtr	tab[3] =
	{
		&Player::moveTo,
		&Player::attackHim,
		&Player::doSkill		
	};

	--action;
	if (action < 0 || action > 2)
		return ;
	(this->*tab[action])(display->getStart(), display->getWhere());
}