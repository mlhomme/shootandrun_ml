#include "Map.hh"

Map::Map(const Position& position)
{
	Line				line(position.first, NULL);
	Tab					tab(position.second, line);
	Random 				random;
	Position 			pos;
	int					type;

	pos.first = 0;
	pos.second = 0;
	if (position.first < 20 || position.first > 1000 || position.second < 20 || position.second > 1000 || position.second != position.first)
	{
		std::string					error("Map have to be in 20 ~ ~ 1000 and have to be a squarre");
		throw error;
	}
	map = tab;
	while (pos.second != position.second)
	{
		type = random.my_random(6);
		map[pos.second][pos.first] = new Case(type);
		++(pos.first);
		if (pos.first == position.first)
		{
			pos.first = 0;
			++(pos.second);
		}
	}
}

Map::~Map()
{
	
}

void			Map::add(AUnit *ptr)
{
	const Position&	pos = ptr->getPosition();

	map[pos.second][pos.first]->add(ptr);
}

void			Map::erase(AUnit *ptr)
{
	const Position&	pos = ptr->getPosition();

	map[pos.second][pos.first]->erase();
}

const Tab&			Map::getTab() const
{
	return (map);
}

const ACase*		Map::getCase(const Position& position) const
{
	return (map[position.second][position.first]);
}

std::ostream&		operator<<(std::ostream& flux, const Map& map)
{
	Position 		pos;
	const Tab& 		tab = map.getTab();

	pos.first = 0;
	pos.second = 0;
	while (pos.second != static_cast<int>(tab.size()))
	{
		flux << tab[pos.second][pos.first]->getType() << " ";
		++(pos.first);
		if (pos.first == static_cast<int>(tab[0].size()))
		{
			flux << std::endl;
			pos.first = 0;
			++(pos.second);
		}
	}
	return (flux);
}