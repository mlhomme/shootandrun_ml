#include "Case.hh"

#include <iostream>

Case::Case(int type) : ACase()
{
	type_to_caract				tab[6] =
	{
		{PLAIN , 0, 1, 0, 1},
		{MONTAIN , 1, 0, 2, 0},
		{RIVER , 0, 1, 1, 0},
		{ROAD, 2, 0, 0, 0},
		{RUINS, 0, 3, 1, 0},
		{FOREST, 0, 2, 1, 2}
	};	

	if (type < 0 || type > 5)
	{
		std::string			error("Invalid type of case");
		throw error;
	}

	this->type = type;

	range = tab[type].range;
	defense = tab[type].defense;
	speed = tab[type].speed;
	attack = tab[type].attack;

	ptr = NULL;
}

Case::~Case()
{

}

void				Case::add(AUnit *ptr)
{
	this->ptr = ptr;
}

void				Case::erase()
{
	this->ptr = NULL;
}

int					Case::getType() const
{
	return (type);
}

AUnit				*Case::getPtr() const
{
	return (ptr);
}

int					Case::getSpeed() const
{
	return (speed);
}

int					Case::getRange() const
{
	return (range);
}

int					Case::getDefense() const
{
	return (defense);
}

int					Case::getAttack() const
{
	return (attack);
}