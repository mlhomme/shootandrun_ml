#include <sstream>

#include "PlayerManager.hh"

int						main(int ac, char **av)
{
	(void)av;
	if (ac != 1 && ac != 3)
	{
		std::cerr << "Usage: ./shoot or ./shoot x y" << std::endl;
		return (1);
	}
	try
	{
		Position 		pos;
		if (ac == 1)
		{
			pos.first = 20;
			pos.second = 20;	
		}
		else
		{
			std::stringstream		ss;
			ss << av[1];
			ss >> pos.first;
			ss.clear();
			ss << av[2];
			ss >> pos.second;
		}

		PlayerManager 	Manager(pos);
		Manager.game();	
	}
	catch (const std::string &error)
	{
		std::cerr << error << std::endl;
	}
}