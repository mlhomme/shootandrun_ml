NAME		= shootandrun

SRC		= src/Case.cpp \
		  src/Berserk.cpp \
		  src/Display.cpp \
		  src/main.cpp \
		  src/Map.cpp \
		  src/Missile.cpp \
		  src/Player.cpp \
		  src/PlayerManager.cpp \
		  src/Random.cpp \
		  src/Tank.cpp \
		  src/Militien.cpp \
		  src/Plane.cpp \
		  src/Rocket.cpp \
		  src/Unit.cpp

OBJ		= $(SRC:.cpp=.o)

CXXFLAGS	=  -Wall -Wextra -W -Werror -I includes

all:		$(NAME)

$(NAME):	$(OBJ)
		g++ -o $(NAME) $(OBJ) -L/usr/local/lib -lallegro -lallegro_image -lallegro_ttf -lallegro_font -lallegro_audio -lallegro_acodec

clean:
		rm -f $(OBJ)

fclean:		clean
		rm -f $(NAME)

re:		fclean all

.PHONY:		all clean fclean re